<script>
	function enviardados(){

	if( (document.dados.data_fim.value=="" || document.dados.data_fim.value.length < 8) && (document.dados.status.value == 4) )
	{
	alert( "Preencha campo Data Fim corretamente!" );
	document.dados.data_fim.focus();
	return false;
	}  
	return true;
	}
</script>

<?php
	 if(empty($id)){$executar = "Adicionar"; $acao=BASE_URL .'atividade/cadastrar/' ;}else {$executar = "Editar"; $acao=BASE_URL .'atividade/atualizar/'.$atividade['id'];;}
?>
<div class="container-fluid">
	<h1><?php echo $executar;?> </h1>

	<form method="POST" name="dados" action="<?php echo $acao; ?>" onSubmit="return enviardados();">
		<div class="form-group">
			<label for="nome">Nome:</label>
			<input type="text" 	name="nome" maxlength="255" required="" value="<?php if(!empty($atividade)) echo utf8_encode($atividade['nome']); ?>" class="form-control">
		</div>
				
		<div class="form-group">
			<label for="descricao">Descrição:</label>
			<textarea name="descricao" maxlength="600" required="" class="form-control"><?php if(!empty($atividade['descricao'])) echo utf8_encode($atividade['descricao']); ?></textarea>			
		</div>

		<div class="form-group">
		<label for="data_inicio">Data Inicio:</label>
		<input type="date" name="data_inicio" required="" value="<?php if(!empty($atividade)) echo $atividade['data_inicio'] ?>">
		<label for="data_fim">Data Fim:</label>
		<input type="date" name="data_fim" value="<?php if(!empty($atividade['data_fim'])) echo $atividade['data_fim'] ?>">
		</div>
		
		<div class="form-group">
			<label for="status">Status:</label>
			<select id="status" name="status" class="form-control">
				<option></option>
					<?php foreach($status as $sta): ?>
					<option value="<?php echo $sta['id']; ?>" <?php if(!empty($atividade['status'])){echo ($sta['id']==$atividade['status'])?'selected="selected"':'';}  ?>><?php echo utf8_encode($sta['nome']); ?></option>
					<?php endforeach; ?>
			</select>
		</div>


		<div class="form-group">
			<label for="situacao">Situação:</label>
			<select id="situacao" name="situacao" class="form-control" required="">
				<option></option>
				<option value="1" <?php if(!empty($id)){echo ($atividade['situacao']=='1')?'selected="selected"':'';}?>>Ativo</option>
				<option value="0" <?php if(!empty($id)){echo ($atividade['situacao']=='0')?'selected="selected"':''; }?>>Inativo</option>
			</select>
		</div>

		<input type="submit" value="<?php echo $executar;?>">
	</form>
</div>