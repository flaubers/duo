<div class="container-fluid">	

	<div class="row">
		<div class="col-sm-3">
			<h4>Pesquisa Avançada</h4>
			<form method="GET">
				<div class="form-group">
					<label for="status">Status:</label>
					<select id="status" name="filtros[status]" class="form-control">
						<option></option>
						<?php foreach($status as $sta): ?>
						<option value="<?php echo $sta['id']; ?>" <?php echo ($sta['id']==$filtros['status'])?'selected="selected"':''; ?>><?php echo utf8_encode($sta['nome']); ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="form-group">
					<label for="situacao">Situação:</label>
					<select id="situacao" name="filtros[situacao]" class="form-control">
						<option></option>
						<option value="1" <?php echo ($filtros['situacao']=='1')?'selected="selected"':''; ?>>Ativo</option>
						<option value="0" <?php echo ($filtros['situacao']=='0')?'selected="selected"':''; ?>>Inativo</option>
					</select>
				</div>				

				<div class="form-group">
					<input type="submit" class="btn btn-info" value="Buscar" />
				</div>
			</form>

		</div>
		<div class="col-sm-9">
			<h4>Atividades</h4>
			<table class="table table-striped">
				<tbody>
					<?php foreach($atividades as $atividade): ?>
					<tr <?php if($atividade['status'] == 4){ echo "bgcolor='#0496d8'";} ?> >
						<td><?php echo utf8_encode($atividade['id']); ?></td>
						<td><?php echo utf8_encode($atividade['nome']); ?></td>
						<td><?php echo utf8_encode($atividade['descricao']); ?></td>						
						<td>							
							<?php echo utf8_encode($atividade['descstatus']); ?>
						</td>
						<td><?php echo utf8_encode($atividade['descsituacao']); ?></td>
						<td><button class="btn btn-warning glyphicon " <?php if($atividade['status'] == 4){ echo "disabled"; $UrlEditar = "#"; }else{ $UrlEditar = BASE_URL .'atividade/editar/'.$atividade['id']; }?> /><a href="<?php echo $UrlEditar; ?>"?> Editar</a></button></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<div class="form-group">
					<button class="btn btn-info" /><a href="<?php echo BASE_URL; ?>atividade/adicionar/" url="<?php echo BASE_URL; ?>/atividade/adicionar/">Incluir Nova Atividade</a></button>
				</div>

			
		</div>
	</div>


</div>