<?php
class homeController extends controller {

	public function index() {
		$dados = array();

		$atividades = new Atividades();
		$status = new Status();
		

		$filtros = array(			
			'status' => '',
			'situacao' => ''
		);
		if(isset($_GET['filtros'])) {
			$filtros = $_GET['filtros'];			
		}

		$total_atividades = $atividades->getTotalAtividades($filtros);
		

		$p = 1;
		if(isset($_GET['p']) && !empty($_GET['p'])) {
			$p = addslashes($_GET['p']);
		}

		$por_pagina = 2;
		$total_paginas = ceil($total_atividades / $por_pagina);

		$atividades = $atividades->getUltimosAnuncios($p, $por_pagina, $filtros);

		$status = $status->getLista();

		$dados['total_atividades'] = $total_atividades;
		//$dados['total_usuarios'] = $total_usuarios;
		$dados['status'] = $status;
		$dados['filtros'] = $filtros;
		$dados['atividades'] = $atividades;
		$dados['total_paginas'] = $total_paginas;
		$dados['p'] = $p;

		$this->loadTemplate('home', $dados);

	}

}