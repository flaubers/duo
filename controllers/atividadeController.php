<?php
class atividadeController extends controller {

	public function adicionar(){		
		$status = new Status();
		$status = $status->getLista();
		$dados['status'] = $status;
		$this->loadTemplate('atividade', $dados);

	}

	public function cadastrar(){
		$dados = array();

		$atividade = new Atividades();
		$status = new Status();

		$status = $status->getLista();

		$filtros = array(			
			'status' => '',
			'situacao' => ''
		);

		if(!empty($_POST['nome'])){
			$nome = utf8_decode($_POST['nome']);
			$descricao = utf8_decode($_POST['descricao']);
			$data_inicio = $_POST['data_inicio'];
			$situacao = $_POST['situacao'];
			$data_fim = $_POST['data_fim'];
			$status = $_POST['status'];
	 
			$atividade->adicionar($nome, $descricao, $data_inicio, $status, $situacao, $data_fim);			
	
		}

		$p = 1;
		if(isset($_GET['p']) && !empty($_GET['p'])) {
			$p = addslashes($_GET['p']);
		}

		$total_atividades = $atividade->getTotalAtividades($filtros);
		$por_pagina = 2;
		$total_paginas = ceil($total_atividades / $por_pagina);

		$atividades = $atividade->getUltimosAnuncios($p, $por_pagina, $filtros);

		

		$dados['status'] = $status;
		$dados['filtros'] = $filtros;
		$dados['atividades'] = $atividades;

		$this->loadTemplate('home', $dados);	
	}

	public function editar($id){
		$atividades = new Atividades();
		$status = new Status();

		$status = $status->getLista();

		$dados['id']=$id;		
		$dados['atividade'] = $atividades->getAtividade($id);
		$dados['status'] = $status;

		$this->loadTemplate('atividade', $dados);


	}

	public function atualizar($id){
		$dados = array();

		$atividade = new Atividades();
		$status = new Status();

		$dados['status'] = $status->getLista();

		$filtros = array(			
			'status' => '',
			'situacao' => ''
		);

		if(!empty($_POST['nome'])){
			$nome = utf8_decode($_POST['nome']);
			$descricao = utf8_decode($_POST['descricao']);
			$data_inicio = $_POST['data_inicio'];
			$situacao = $_POST['situacao'];
			$data_fim = $_POST['data_fim'];
			$status = $_POST['status'];
	 
			$atividade->registrar($nome, $descricao, $data_inicio, $status, $situacao, $id, $data_fim);			
	
		}
		$total_atividades = $atividade->getTotalAtividades($filtros);
		$p = 1;
		if(isset($_GET['p']) && !empty($_GET['p'])) {
			$p = addslashes($_GET['p']);
		}

		$por_pagina = 2;
		$total_paginas = ceil($total_atividades / $por_pagina);

		$atividades = $atividade->getUltimosAnuncios($p, $por_pagina, $filtros);		

		
		$dados['filtros'] = $filtros;
		$dados['atividades'] = $atividades;

		$this->loadTemplate('home', $dados);
	}




}

