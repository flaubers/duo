
   
  CREATE TABLE atividades (
    id int NOT NULL AUTO_INCREMENT,
    nome varchar(255) NOT NULL,
    descricao text(600) not null,
    data_inicio date not null,
	data_fim date,
	status int,
	situacao tinyint(1),
    PRIMARY KEY (id)
); 

  
   CREATE TABLE status (
  id INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(45) NULL,
  PRIMARY KEY (id))
;

INSERT INTO `duo`.`status` (`nome`) VALUES ('Pendente');
INSERT INTO `duo`.`status` (`nome`) VALUES ('Em Desenvolvimento');
INSERT INTO `duo`.`status` (`nome`) VALUES ('Em Teste');
INSERT INTO `duo`.`status` (`nome`) VALUES ('Concluído');

ALTER TABLE `duo`.`atividades` 
ADD INDEX `fk_codstatus_status_idx` (`status` ASC);
;
ALTER TABLE `duo`.`atividades` 
ADD CONSTRAINT `fk_codstatus_status`
  FOREIGN KEY (`status`)
  REFERENCES `duo`.`status` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
