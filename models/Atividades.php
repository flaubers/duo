<?php
class Atividades extends model {
	public function getTotalAtividades($filtros) {
		$filtrostring = array('1=1');
		if(!empty($filtros['status'])) {
			$filtrostring[] = 'status = :status';
		}
		if(!empty($filtros['situacao'])) {
			$filtrostring[] = 'situacao = :situacao';
		}		

		$sql = $this->db->prepare("SELECT COUNT(*) as Total FROM atividades WHERE ".implode(' AND ', $filtrostring));

		if(!empty($filtros['status'])) {
			$sql->bindValue(':status', $filtros['status']);
		}
		if(!empty($filtros['situacao'])) {
			$sql->bindValue(':situacao', $filtros['situacao']);			
		}

		$sql->execute();
		$row = $sql->fetch();

		return $row['Total'];
	}

	public function getUltimosAnuncios($page, $perPage, $filtros) {
		$offset = ($page - 1) * $perPage;

		$array = array();


		$filtrostring = array('1=1');
		if(strlen($filtros['status']) > 0) {
			$filtrostring[] = 'status = '.$filtros['status'];
		}				
		if(strlen($filtros['situacao']) > 0 ) {
			$filtrostring[] = 'situacao = '.$filtros['situacao'];
		}		

		$sql = $this->db->prepare("SELECT id, nome, descricao, data_inicio, data_fim, if (situacao = 1, 'ATIVO', 'INATIVO') AS descsituacao, (SELECT status.nome from status where status.id = atividades.status) as descstatus, status, situacao  FROM atividades WHERE ".implode(' AND ', $filtrostring));
		
		$sql->execute();

		if($sql->rowCount() > 0) {
			$array = $sql->fetchAll();
		}

		return $array;
	}

	public function adicionar($nome, $descricao, $data_inicio, $status, $situacao, $data_fim=''){
		if(empty($data_fim)){
			$sql = "INSERT INTO atividades (nome, descricao, data_inicio, status, situacao) VALUES (:nome, :descricao, :data_inicio, :status, :situacao)";
		}else{
			$sql = "INSERT INTO atividades (nome, descricao, data_inicio, status, situacao, data_fim) VALUES (:nome, :descricao, :data_inicio, :status, :situacao, :data_fim)";
		}

		
		$sql = $this->db->prepare($sql);
		$sql->bindValue(':nome',$nome);
		$sql->bindValue(':descricao',$descricao);
		$sql->bindValue(':data_inicio',$data_inicio);
		$sql->bindValue(':status',$status);
		$sql->bindValue(':situacao',$situacao);
		if(!empty($data_fim)){$sql->bindValue(':data_fim',$data_fim);}		
		$sql->execute();		
		return true;
	}

	public function getAtividade($id) {
		$array = array();

		$sql = $this->db->prepare("SELECT
			id, nome, descricao, data_inicio, data_fim, situacao, status				
		FROM atividades WHERE id = :id");
		$sql->bindValue(":id", $id);
		$sql->execute();
		$array = $sql->fetch();	

		return $array;
	}

	public function registrar($nome, $descricao, $data_inicio, $status, $situacao, $id, $data_fim){
		if(empty($data_fim)){
			$sql = "UPDATE  atividades  SET nome = :nome, descricao = :descricao, data_inicio = :data_inicio, status = :status, situacao = :situacao WHERE id = :id";
		}else{
			$sql = "UPDATE  atividades  SET nome = :nome, descricao = :descricao, data_inicio = :data_inicio, status = :status, situacao = :situacao, data_fim = :data_fim WHERE id = :id";
		}
		
		$sql = $this->db->prepare($sql);
		$sql->bindValue(':nome',$nome);
		$sql->bindValue(':descricao',$descricao);
		$sql->bindValue(':data_inicio',$data_inicio);
		$sql->bindValue(':status',$status);
		$sql->bindValue(':situacao',$situacao);
		if(!empty($data_fim)){$sql->bindValue(':data_fim',$data_fim);}
		$sql->bindValue(':id',$id);
		$sql->execute();
		return true;
		
	}
}
